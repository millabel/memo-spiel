var karten = [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7];
var historie = [-1, -1];
var click = 0;
var warten = false;
var versuche = 0;
		
function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i -= 1) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}		

function neu(){
	for(var n=0; n<16; n++){
		document.getElementById('a' + n).src = 'freesia.png';
	}
	shuffle(karten);
	click = 0;
	historie = [-1, -1];
	warten = false;
	versuche = 0;
	document.getElementById('msg').innerHTML = 'Fehlversuche: 0';
}
		
		
function umdrehen(){
	document.getElementById('a' + historie[0]).src = 'freesia.png';
	document.getElementById('a' + historie[1]).src = 'freesia.png';
	warten = false;
}
		
		
function oeffnen(nr){
			
	// Klickt der Anwender auf ein gültiges Bild?
	// .search('deckel.jpg') oder indexOf('deckel.jpg');
	var deckel = document.getElementById('a' + nr).src.indexOf('freesia.png'); 
				
	if(deckel != -1 && warten == false) {
					
		historie[click] = nr;
		click++;
					
		if (click == 2){
			var a = historie[0];
			var b = historie[1];
			if(karten[a] != karten[b]){
				setTimeout(umdrehen ,700);
				warten = true;
				versuche++;
				document.getElementById('msg').innerHTML ='Fehlversuche: ' + versuche;
			}
			
			click = 0;				
		}
					
		document.getElementById('a'+ nr).src = karten[nr]+'.png';
	}			
}